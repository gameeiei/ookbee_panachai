<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserLogin;
class MainController extends Controller
{
	public function register(Request $request)
	{
		// print_r($request->all());exit();
		$birthDate = date("Y-m-d", strtotime($request->birthDate));
		$dataSet = array();
		array_push($dataSet,
			array(
				'email' => $request->email,
				'name' => $request->name,
				'lastname' => $request->lastname,
				'address' => $request->address,
				'phone' => $request->phone,
				'gender' => $request->gender,
				'birthDate' => $birthDate,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at'=>date("Y-m-d H:i:s")
			)
		);
		if($request->userId == 0){
			if(UserLogin::insert($dataSet)){
				echo 1;
			}else{
				echo 0;
			}
		}else{
			if(UserLogin::where('id', $request->userId)->update($dataSet[0])){
				echo 1;
			}else{
				echo 0;
			}
		}


	}
	public function getUsers()
	{

		return UserLogin::get();


	}
}
